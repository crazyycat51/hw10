'use strict'

const tabs = document.querySelectorAll('.tabs li');
const paragraphs = document.querySelectorAll('.tabs-content li')

tabs.forEach((tab, tabIndex) => {
    tab.onclick = () => {
        tabs.forEach((item) => {
            item.classList.remove('active');
        })
        tab.classList.add('active');

        paragraphs.forEach((paragraph, paragraphIndex) => {
            if (paragraphIndex === tabIndex) {
                paragraph.style.display = 'block';
            } else {
                paragraph.style.display = 'none';
            }
        })
    }
})